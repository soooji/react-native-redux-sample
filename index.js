/** @format */
import React, {Component} from 'react';
import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';

import {createStore,applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import reducers from './reducers';

import App from './App';

const store = createStore(reducers,applyMiddleware(thunk));
const RNRedux = () => (
    <Provider store={store}><App/></Provider>
  )
AppRegistry.registerComponent(appName, ()=>RNRedux);
