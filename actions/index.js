import axios from 'axios';
export function loadUserInfo() {
    return(dispatch)=>{
        return axios.get('https://jsonplaceholder.typicode.com/users/1')
            .then(Response=>{
                dispatch({
                    type: 'LOAD_USER_INFO',
                    user_info: Response.data
                })
            })
    }
}