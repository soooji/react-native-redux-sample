import React, {Component} from 'react';
import {Dimensions,Platform,Image,Text, View} from 'react-native';
import SvgUri from 'react-native-svg-uri';
const WIN = Dimensions.get('window');
import * as actionCreators from './actions';
function mapStateToProps(state) {
  return state
}

import {connect} from 'react-redux';

class App extends Component {
  constructor(props) {
    super(props);
    this.props.loadUserInfo();
  }
  
  render() {
    return (
      <View>
        <View style={{position:'relative',width:WIN.width,height:300}}>
        <SvgUri
          width={WIN.width}
          height={253}
          style={{position:'absolute',top:0}}
          source={require('./static/yellow-bar.svg')}
        /> 
        <View style={{
          position:'absolute',
          width:'100%',
          paddingTop:15,
          marginTop:20,
          display: 'flex',
	        flexDirection: 'row',
	        flexWrap: 'nowrap',
	        justifyContent: 'space-between',
	        alignItems: 'center',
	        alignContent: 'stretch'}}>
          <Image 
            source={require('./static/img/notif-icon.png')}
            style={{position:'absolute',top:0,width:26,height:28}}
          />
        </View>
        </View>
        <Text>
          Redux is working...
        </Text>
        <Text>
          {this.props.user_info.name}
        </Text>
        <Text>
          {this.props.user_info.email}
        </Text>
      </View>
    );
  }
}
export default connect(mapStateToProps,actionCreators)(App);