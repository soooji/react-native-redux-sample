let defaultState = {
    user_info: {}
}
const mainReducer = (state=defaultState,action) => {
    switch(action.type) {
        case 'LOAD_USER_INFO' :
            return{
                ...state,
                user_info: action.user_info
            }
        default : return{
            ...state
        }
    }
}
export default mainReducer;